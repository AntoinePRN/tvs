/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;
using System.Collections.Generic;

public class WorkFlowManager : MonoBehaviour
{
    public static WorkFlowManager instance;

    public DataSaver data;

    /**
     * On énumère les différents statut de notre workflow
     */
    public enum Status
    {
        S, E, IA, ISAD, NH, HC, HR, DC, Rsymp, Rasymp, Rtot, Vs, Ve, SVS
    }

    /**
     * On déclare ici un dictionnaire qui assigne à chaque statut un nombre de personne
     * Ce nombre est représenté par un double pour toute la partie calculatoire
     * le passage en entier se réalisera au niveau de l'affichage
     */
    public IDictionary<Status, double> effective = new Dictionary<Status, double>()
    {
        {Status.S , 67000000.0},
        {Status.E , 100.0},
        {Status.IA, 0},
        {Status.ISAD, 0},
        {Status.NH, 0},
        {Status.HC, 0},
        {Status.HR, 0},
        {Status.DC, 0},
        {Status.Rsymp, 0},
        {Status.Rasymp, 0},
        {Status.Rtot, 0 },
        {Status.Vs, 0},
        {Status.Ve, 0},
        {Status.SVS, 67000000.0}
    };

    private double popTotaleInitiale = 0;

    /**
     *  !!!!!!!
     *  Les valeurs données ici aux attributs dans le code ne servent qu'à garder une trace.
     *  La valeur effective est renseignée directement sur unity.
     */

    /**
     * On se charge ici de toute la partie affichage dans l'inspecteur
     * On va rentrer les valeurs par défaut au préalable
     * On vient rajouter une mention qui cqche les params public non nécessaire sur Unity pour plus de lisibilité dans le logiciel 
     */
    [Header("Paramètres généraux de la simulation")]
    [HideInInspector] public bool isSimulationRunning = false; //Ce booléan nous servira unqiuement d'indicateur pour savoir l'état de la simulation en false par défaut car la simulation ne tourne pas des le depart
    public int dayNumber = 0;
    public float updateInterval = 0.1f; //L'intervalle de rafraichissement de notre simulation
    public float nbRC = 12; //le nombre de recontre quotidienne
    [HideInInspector] public int indexButton = 0; //L'index souhaité pour le déplacement des graphs

    public const float LOW_SPEED = 0.7f;
    public const float HIGH_SPEED = 0.1f;
    public const float VERY_HIGH_SPEED = 0.02f;
    private float oldSpeed = 0.1f; //Par défaut la même vitesse que le rapide

    //Populationss
    [HideInInspector] public double Spop = 67000000.0f;
    [HideInInspector] public double Epop = 100.0f;

    //La mention SerializeField permet d'éditer un attribut privé sur unity
    //Comme ça dans le code il est clair pour nous que ces attributs sont privés mais éditable dans la simulation
    [Header("Probabilité")]
    [SerializeField] private float pIA = 0.3f;
    [SerializeField] private float pHC = 0.064f;
    [SerializeField] private float pHR = 0.016f;
    [SerializeField] private float pDCHC = 0.08f;
    [SerializeField] private float pDCHR = 0.4f;
    private float pCT = 0.041f;
    private float oldPCT = 0.041f;
    [SerializeField] private float BASE_PCT = 0.041f;
    //pNH privé car il dépend de pHC et pHR on va l'initialiser dans start l165 => pNH = 1 - (pHR + pHC);
    private float pNH; 

    [Header("Temps dans compartiment")]
    public float tE = 5.1f;
    public float tISAD = 3f;
    public float tIA = 7f;
    public float tNH = 4f;
    public float tHC = 17.3f;
    public float tHR = 20f;

    [Header("Infectiosité")]
    public float inf_Asymp = 0.5f;
    public float inf_Symp = 1f;

    /**
     * Les mesures sont de base à 0 qui correspond au mode "aucune mesure"
     */
    [Header("Paramètres des mesures")]
    public float pPortMasqueDefaut = 0f;
    public float pPortMasqueNH = 0f;

    public float pourcentReductionContactDefaut = 0;
    public float pourcentReductionContactsNH = 0;

    public float pourcentReductionContagiositeMasque = 0;

    public int dateDebutMesuresBarriere = 100;
    public int dateFinMesuresBarriere = 1800;

    //Ce booléen privé va permettre de savoir si les mesures barrières sont activé
    //Il est différent du mode mesure Barrière qui lui représente la volonté de l'utilisateur de les activers -> il peut etre true
    //alors qu'elle ne sont pas encore active (avant dateDebutMesuresBarriere)
    private bool mesureBarriereIsRunning = false;

    [Header("Paramètres vaccination")]
    public int jourDebutVaccination = 390;
    public int jourFinVaccination = 750;
    public float pEfficaciteVaccin = 0.90f;
    private double nbVaccinsParJour = 0;

    [Header("Paramètres confinement")]
    public int dateDebutPremierConfinement = 100;
    public int nombreDeConfinements = 3;
    public int dureeDesConfinements = 45;
    public int dureeEntreDeuxConfinements = 120; 
    private int nombreConfinementFinit = 0;
    public  bool confinementIsRunning = false;

    [Header("Paramètres variant")]
    public int dateDebutVariant = 390;
    public float facteurContagiosite = 1.6f;


    [Header("Activation des modes")]
    public bool modeVaccination = false;
    public bool modeMesureBarriere = false;
    public bool modeConfinementProgramme = false;
    public bool modeConfinementBouton = false;
    public bool modeVariant = false;

    //Différent cumuls 
    [HideInInspector] public int cumulJourEnConfinement = 0;
    [HideInInspector] public double cumulPassageHopital = 0;

    /**
     * On va déclarer ici nos attributs privés qui ne serviront qu'à l'intérieur de ce programme.
     * Il s'agit de tout les différent DX
     * Voir si possibilité de déclarer moins d'attribut pour mieux tourner
     */
    private double dS;
    private double dE;
    private double dIA;
    private double dISAD;
    private double dNH;
    private double dHC;
    private double dHR;
    private double dDC;
    private double dRsymp;
    private double dRasymp;

    private void Awake()
    {
        //Nous permet de génerer une instance static de notre classe pour y accéder partout dans le code
        if (instance != null)
        {
            Debug.LogError("Plusieurs instance de WorkflowManager");
            return;
        }

        instance = this;
        //Permet de garder lôbjet WorkFlowManager dans unity pour le transporter dans les différentes scènes
        DontDestroyOnLoad(this);
    }

    /**
     * La fonction Start() est appelé au début de la simulation
     */
    private void Start()
    {
        //On attribut les effectifs initiaux 
        effective[Status.S] = Spop; 
        effective[Status.SVS] = Spop;
        effective[Status.E] = Epop;

        //On définit pnh en fonction de phr et phc
        pNH = 1 - (pHR + pHC);

        //On attribut le facteur de contagiosité de base 
        pCT = BASE_PCT;
        oldPCT = BASE_PCT;

        //On vient calculer la population totale au début de la simulation pour les calculs de la vaccination
        popTotaleInitiale = effective[Status.SVS] + effective[Status.Rtot]
            + effective[Status.E] + effective[Status.IA] + effective[Status.ISAD]
            + effective[Status.HC] + effective[Status.HR] + effective[Status.NH]
            + effective[Status.DC];
    }

    /**
     * Méthode invoqué tout les updateInterval qui va nous servir a calculer les différents dx
     */
    private void UpdateInterval()
    {
        //On vérifie que la variable Data a bien été renseigné
        //On vient renseigner les data pour j-1 donc on le fait avant les calculs de DX
        if (data != null)
            data.AddNewValue(effective);

        //Si jamais aucun confinement ne tourne 
        if (!confinementIsRunning)
        {
            //Soit il faut activer un confinement
            if (IsConfinementSupposedToRun())
            {
                ActiverConfinement(true);
            }
            else //Soit on doit vérifier les mesures barrières
            {
                if (!modeMesureBarriere && mesureBarriereIsRunning)
                    ActiverMesureBarriere(false);
                if (modeMesureBarriere && dayNumber >= dateDebutMesuresBarriere && dayNumber < dateFinMesuresBarriere && !mesureBarriereIsRunning)
                    ActiverMesureBarriere(true);
                //Si on arrive a la fin de la date des mesures barrières et qu'elle tournaient
                if (dayNumber >= dateFinMesuresBarriere && mesureBarriereIsRunning)
                    ActiverMesureBarriere(false);
            }
        }
        else //Si le confinement tourne on incrémente le cumul de jour en confinement 
        {
            cumulJourEnConfinement++;
        }

        //Si le confinement tourne alors qu'il faudrait l'arreter
        if(confinementIsRunning && !IsConfinementSupposedToRun())
        {
            cumulJourEnConfinement--;
            //On désactive le confinement 
            ActiverConfinement(false);
            if (!modeMesureBarriere && mesureBarriereIsRunning)
                ActiverMesureBarriere(false);
            if (modeMesureBarriere && dayNumber >= dateDebutMesuresBarriere && dayNumber < dateFinMesuresBarriere && !mesureBarriereIsRunning)
                ActiverMesureBarriere(true);
            //Si on arrive a la fin de la date des mesures barrières et qu'elle tournaient
            if (dayNumber >= dateFinMesuresBarriere && mesureBarriereIsRunning)
                ActiverMesureBarriere(false);
        }

        //Si le mode variant est censé démarrer et que le pCT n'est pas encore modifié
        //On stock constamment la vieille valeur de pCT car un utilisateur peut changer plusieurs fois de variants 
        if(modeVariant && dayNumber >= dateDebutVariant)
        {
            if (!(pCT.CompareTo(oldPCT * facteurContagiosite) == 0))
            {
                oldPCT = pCT;
                pCT *= facteurContagiosite;
            }
       
        }

        //On applique les changements d'effectifs
        CalculateDX();
        ApplicateDX();

        //On vient réaliser la vaccination en "fin de journée" à condition d'être dans la période de vaccination
        //La vaccination continuera tant que tout les S n'ont pas été vacciné
        //On veut également que la vaccination soit activé ce qui correspond à un intervalle différent de 0
        //Revoir la condition pour remplacer par un booléen
        if (dayNumber >= jourDebutVaccination && (effective[Status.S] >= 0.1 || effective[Status.Rasymp] >= 0.1) && modeVaccination)
            Vaccination();

        //On actualise en fin de journé les effectifs d'aggrégat qui regroupent plusieurs compartiments
        effective[Status.Rtot] = effective[Status.Rasymp] + effective[Status.Rsymp] + effective[Status.Ve];
        effective[Status.SVS] = effective[Status.S] + effective[Status.Vs];

        //Finalement on incrémente notre variable de temps 
        dayNumber++;
    }

    /**
     * Permet de calculer les différents DX à chaque itération 
     */
    private void CalculateDX()
    {

        //dE
        double dEplus = 0f;
        if (effective[Status.ISAD] + effective[Status.IA] + effective[Status.NH] > 1.0f)
        {
            dEplus += effective[Status.IA] * CalculNCS(Status.IA);
            dEplus += effective[Status.ISAD] * CalculNCS(Status.ISAD);
            dEplus += effective[Status.NH] * CalculNCS(Status.NH);
        }
        double dEmoins = effective[Status.E] / tE;
        dE = dEplus - dEmoins;

        //dS
        dS = -dEplus;

        //dIA
        double dIAplus = pIA * dEmoins;
        double dIAmoins = effective[Status.IA] / tIA;
        dIA = dIAplus - dIAmoins;

        //dISAD
        double dISADplus = (1 - pIA) * dEmoins;
        double dISADmoins = effective[Status.ISAD] / tISAD;
        dISAD = dISADplus - dISADmoins;

        //dNH
        double dNHplus = pNH * dISADmoins;
        double dNHmoins = effective[Status.NH] / tNH;
        dNH = dNHplus - dNHmoins;

        //dHC
        double dHCplus = pHC * dISADmoins;
        double dHCmoins = effective[Status.HC] / tHC;
        dHC = dHCplus - dHCmoins;
        cumulPassageHopital += dHCplus;

        //dHR
        double dHRplus = pHR * dISADmoins;
        double dHRmoins = effective[Status.HR] / tHR;
        dHR = dHRplus - dHRmoins;
        cumulPassageHopital += dHRplus;

        //dDC
        dDC = pDCHC * dHCmoins + pDCHR * dHRmoins;

        //dR
        dRsymp = (1 - pDCHC) * dHCmoins + (1 - pDCHR) * dHRmoins + dNHmoins;
        dRasymp = dIAmoins;
    }

    /**
     * Méthode permettant d'effectuer le calcul de NCS pour la contribution à dEplus 
     * @param s le statut dont on veut le NCS
     * 
     * @return la valeur de NCS
     */
    private double CalculNCS(Status s)
    {
        //Condition ternaire pour définir l'infectiosité en fonction du status pour le calcul
        float inf = s == Status.IA ? inf_Asymp : inf_Symp;

        float pPortMasque = s == Status.NH ? pPortMasqueNH : pPortMasqueDefaut;
        float pourcentageReductionContact = s == Status.NH ? pourcentReductionContactsNH : pourcentReductionContactDefaut;

        //La population totale qui peut rentrer en contact avec un individu
        double N = effective[Status.ISAD] + effective[Status.IA] + effective[Status.NH] +
                effective[Status.E] + effective[Status.S] + effective[Status.Rasymp] + effective[Status.Rsymp]
                + effective[Status.Vs] + effective[Status.Ve];

        //Nombre de rencontre avec un individu de S
        double NRS = nbRC * (1 - pourcentageReductionContact / 100) * effective[Status.S] / N;

        //Nombre de contagion parmi les rencontres NRS
        double NCS = NRS * (
            pPortMasque * (1 - pPortMasque) * (1 - (pourcentReductionContagiositeMasque / 100.0f)) * pCT * inf
            + (pPortMasque * pPortMasque) * (1 - (pourcentReductionContagiositeMasque / 100.0f)) * (1 - (pourcentReductionContagiositeMasque/100.0f)) * pCT * inf
            + ((1 - pPortMasque) * pPortMasque * (1 - (pourcentReductionContagiositeMasque / 100.0f)) * pCT * inf)
            + (1 - pPortMasque) * (1 - pPortMasque) * pCT * inf
            );

        return NCS;
    }

    /**
     * Méthode qui va nous permettre de réaliser les différents opération liées à la vaccination
     */ 
    private void Vaccination()
    {
        //Le nombre de vaccin par jour est definit par la ééthode en dessous qui est appele dans le settingsManager lorsque l'user rentre ses dates de vaccin
        //Si jamais la population restante à vacciner est inférieur ou égale au nombre de vaccin quotidien
        double popRestantAVacciner = effective[Status.S] + effective[Status.E] + effective[Status.IA] + effective[Status.Rasymp];
        if (nbVaccinsParJour >= popRestantAVacciner)
        {
            //Dans un premier temps on calcul unqiuement l'effectif des personnes saines
            double dVs = effective[Status.S] * pEfficaciteVaccin;
            effective[Status.Vs] += dVs;
            effective[Status.S] -= dVs;

            //Puis on calcul l'effectif des personnes qui ont été en contact avec le virus sans le savoir
            double dEmoins = effective[Status.E] * pEfficaciteVaccin;
            effective[Status.E] -= dEmoins;
            double dIAmoins = effective[Status.IA] * pEfficaciteVaccin;
            effective[Status.IA] -= dIAmoins;
            double dRAmoins = effective[Status.Rasymp] * pEfficaciteVaccin;
            effective[Status.Rasymp] -= dRAmoins;
            effective[Status.Ve] += (dEmoins + dIAmoins + dRAmoins);
        }
        else //Si la population à vacciner est supérieure aux nb de vaccin quotidien
        {
            //Raisonnement similaire au premier bloc du if
            double dVs = (nbVaccinsParJour * effective[Status.S] / popRestantAVacciner) * pEfficaciteVaccin;
            effective[Status.Vs] += dVs;
            effective[Status.S] -= dVs;

            double dEmoins = (nbVaccinsParJour * effective[Status.E] / popRestantAVacciner) * pEfficaciteVaccin;
            effective[Status.E] -= dEmoins;
            double dIAmoins = (nbVaccinsParJour * effective[Status.IA] / popRestantAVacciner) * pEfficaciteVaccin;
            effective[Status.IA] -= dIAmoins;
            double dRAmoins = (nbVaccinsParJour * effective[Status.Rasymp] / popRestantAVacciner) * pEfficaciteVaccin;
            effective[Status.Rasymp] -= dRAmoins;
            effective[Status.Ve] += dEmoins + dIAmoins + dRAmoins;
        }
    }

    /**
     * Méthode qui permet d'ajuster la durée de la vaccination après la modification des paramètres
     */
    public void CalculDureeVaccination()
    {
        float dureeVaccination = jourFinVaccination - jourDebutVaccination;
        nbVaccinsParJour = popTotaleInitiale / dureeVaccination;
    }

    //Cette fonction permet d'appliquer tout les DX
    private void ApplicateDX()
    {
        //Le faire de manière + propre par la suite ?
        effective[Status.S] += dS;
        effective[Status.E] += dE;
        effective[Status.IA] += dIA;
        effective[Status.ISAD] += dISAD;
        effective[Status.NH] += dNH;
        effective[Status.HC] += dHC;
        effective[Status.HR] += dHR;
        effective[Status.DC] += dDC;
        effective[Status.Rsymp] += dRsymp;
        effective[Status.Rasymp] += dRasymp;
    }

    /**
     * Permet de stopper la simulation en stoppant l'invoke actuel
     */
    public void PauseSimulation()
    {
        isSimulationRunning = false;
        CancelInvoke();
    }


    /**
     * Permet de relancer la simulation
     */
    public void ResumeSimulation()
    {
        isSimulationRunning = true;
        InvokeRepeating(nameof(UpdateInterval), updateInterval, updateInterval);
    }

    /**
     * Cette méthode va permettre de réinitialiser les populations initiales lorsque on appuie sur le
     * bouton restart du bandeau
     */
    public void RestartButton()
    {
        //On arrête la simulation si nécessaire 
        if (isSimulationRunning)PauseSimulation();
        //On remet le jour à 0;
        dayNumber = 0;
        //On nettoie l'effectif
        effective.Clear();
        //On replace l'effectif à la base 
        effective = new Dictionary<Status, double>()
        {
            {Status.S , Spop},
            {Status.E , Epop},
            {Status.IA, 0},
            {Status.ISAD, 0},
            {Status.NH, 0},
            {Status.HC, 0},
            {Status.HR, 0},
            {Status.DC, 0},
            {Status.Rsymp, 0},
            {Status.Rasymp, 0},
            {Status.Rtot, 0 },
            {Status.Vs, 0},
            {Status.Ve, 0},
            {Status.SVS, Spop}
        };
        //on nettoie l'historique
        data.ClearAll();

        //On remet les cumuls à 0
        cumulPassageHopital = 0;
        cumulJourEnConfinement = 0;
        nombreConfinementFinit = 0;

        //On désactive le confinement si jamais il était actif
        modeConfinementBouton = false;

        //On désactive toutes les mesures et on remet la valeur de pCT à sa valeur de base (car modifié par le variant)
        ActiverMesureBarriere(false);
        ActiverConfinement(false);

        pCT = BASE_PCT;
        oldPCT = BASE_PCT;

        popTotaleInitiale = effective[Status.SVS] + effective[Status.Rtot]
            + effective[Status.E] + effective[Status.IA] + effective[Status.ISAD]
            + effective[Status.HC] + effective[Status.HR] + effective[Status.NH]
            + effective[Status.DC];
    }

    /**
     * Cette méthode va permettre d'activer/desactiver les mesures barrières
     */
    private void ActiverMesureBarriere(bool active)
    {
        mesureBarriereIsRunning = active;
        if (active)
        {
            Debug.Log("MB ON");
            pPortMasqueDefaut = 0.5f;
            pPortMasqueNH = 0.9f;
            pourcentReductionContactsNH = 80.0f;
            pourcentReductionContagiositeMasque = 30.0f;
        }
        else
        {
            Debug.Log("MB OFF");
            pPortMasqueDefaut = 0f;
            pPortMasqueNH = 0f;
            pourcentReductionContactsNH = 0f;
            pourcentReductionContagiositeMasque = 0f;
        }
    }

    /**
     * Cette méthode va permettre d'activer ou non le confinement
     * C'est simplement une activation des mesures barrière avec une réduction des contacts étendu à toute la pop
     */
    private void ActiverConfinement(bool active)
    {
        confinementIsRunning = active;
        if (active)
        {
            Debug.Log("CONF ON");
            ActiverMesureBarriere(active);
            pourcentReductionContactDefaut = 70f;
        }
        else
        {
            Debug.Log("CONF OFF");
            ActiverMesureBarriere(active);
            pourcentReductionContactDefaut = 0f;
        }
    }

    /**
     * Cette méthode permet de savoir si le confinement est supposé tourner ou non
     */
    private bool IsConfinementSupposedToRun()
    {
        //ON va distinguer en fonction du mode de confinement
        if (modeConfinementProgramme) {
            //On rajoute par sécurité pour éviter une coéxistance des deux modes
            modeConfinementBouton = false;

            //On distingue ensuite en fonction du nombres de confinement passé
            if(nombreConfinementFinit < nombreDeConfinements)
            {
                //On définit les bornes du confinement actuel
                int bornInf = dateDebutPremierConfinement + nombreConfinementFinit * (dureeDesConfinements + dureeEntreDeuxConfinements);
                int bornSup = bornInf + dureeDesConfinements;
                if (dayNumber >= bornInf && dayNumber <= bornSup)
                {
                    return true;
                }
                else //Si on est en dehors des bornes 
                {
                    //on veut venir incrémenter le nb de confinement si on est au desuss de la borne sup
                    nombreConfinementFinit = dayNumber > bornSup ? nombreConfinementFinit + 1 : nombreConfinementFinit;
                    return false;
                }
            }
            else
            {
                return false;
            }      
        }
        else //Si on est pas en mode confinement programmé
        {
            //Lorsque l'on est pas en conf programmé c'est l'utilisateur qui le rernseigne avec le bouton qui vient directement affecter la valeur du booleen ci-dessous
            return modeConfinementBouton;
        }
    }

    /**
     * Cette méthode va simplement permettre d'altérner de manière binaire la vitesse
     * Il faut bien penser a pause la simulation sinon le changementy ne fera rien 
     */
    public void ChangeSpeed()
    {
        bool wasSimulationRunning = isSimulationRunning;
        PauseSimulation();
        //On redéfinit la vitesse 
        updateInterval = updateInterval == LOW_SPEED ? HIGH_SPEED : LOW_SPEED;
        //On sauvegarde la valeur de vitesse courante pour y revenir après une avance rapide
        oldSpeed = updateInterval;
        if(wasSimulationRunning)
            ResumeSimulation();
    }

    /**
     * Cette méthode permet d'accelerer drastiquement la vitesse de la simulation
     * On ne peut que l'utiliser si la somulation tourne
     */
    public void EnableHighSpeed()
    {
        PauseSimulation();
        updateInterval = VERY_HIGH_SPEED;
        ResumeSimulation();
    }

    /**
     * Cette méthode permet de désactiver l'avance rapide
     */
    public void DisableHighSpeed()
    {
        PauseSimulation();
        updateInterval = oldSpeed;
        ResumeSimulation();
    }
}
