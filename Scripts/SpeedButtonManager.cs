/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//On implémente les interface nécessaire pour détecter les appuis sur les boutons 
public class SpeedButtonManager : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    private WorkFlowManager wm;

    public Button button;
    public GameObject RText;
    public GameObject LText;

    private bool isPointerDown = false;
    private bool longPressTriggered = false;
    private float timePressStarted;

    [SerializeField]
    [Tooltip ("Durée nécessaire pour détecter l'appui long")]
    private float dureAppui = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        wm = WorkFlowManager.instance;
        button.onClick.AddListener(delegate { OnClick(); });
        RText.SetActive(wm.updateInterval == WorkFlowManager.LOW_SPEED);
        LText.SetActive(wm.updateInterval == WorkFlowManager.HIGH_SPEED);
    }

    private void OnClick()
    {
        //On ne veut changer l'état que si c'est un appui court
        if (!longPressTriggered)
        {
            wm.ChangeSpeed();
            RText.SetActive(wm.updateInterval == WorkFlowManager.LOW_SPEED);
            LText.SetActive(wm.updateInterval == WorkFlowManager.HIGH_SPEED);
        }
        
    }

    private void Update()
    {
        if (isPointerDown && !longPressTriggered)
        {
            if (Time.time - timePressStarted > dureAppui)
            {
                if(wm.isSimulationRunning && wm.updateInterval != WorkFlowManager.VERY_HIGH_SPEED)
                {
                    longPressTriggered = true;
                    wm.EnableHighSpeed();
                }
            }
        }

        if(!isPointerDown && longPressTriggered && wm.updateInterval == WorkFlowManager.VERY_HIGH_SPEED)
        {
            longPressTriggered = false;
            wm.DisableHighSpeed();
        }

        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isPointerDown = true;
        timePressStarted = Time.time;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPointerDown = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerDown = false;
    }
}
