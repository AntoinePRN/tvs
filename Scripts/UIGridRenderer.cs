/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine.UI;
using UnityEngine;
/**
 * Script qui permet de génnérer facilement une grille 
 * Ce dernier est destiné à l'affichage d'une échelle dynamique pour nos différents graph
 */
public class UIGridRenderer : Graphic
{
    [Header("Attribut de la grille")]
    public Vector2Int gridSize = new Vector2Int(1, 1);
    public float thickness = 10f;

    private float widht;
    private float height;
    private float cellWidht;
    private float cellHeight;


    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();

        widht = rectTransform.rect.width;
        height = rectTransform.rect.height;

        cellWidht = widht / (float)gridSize.x;
        cellHeight = height / (float)gridSize.y;


        int count = 0;
        //On vient dessiner chaque case
        for (int y = 0; y < gridSize.y; y++)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                DrawCell(x, y, count, vh);
                count++;
            }
        }
    }

    private void DrawCell(int x, int y, int index, VertexHelper vh)
    {
        float xPos = cellWidht * x;
        float yPos = cellHeight * y;

        UIVertex vertex = UIVertex.simpleVert;
        vertex.color = color;

        //Vertex composant le tour du cadre
        vertex.position = new Vector3(xPos, yPos);
        vh.AddVert(vertex);

        vertex.position = new Vector3(xPos, yPos + cellHeight);
        vh.AddVert(vertex);

        vertex.position = new Vector3(xPos + cellWidht, yPos + cellHeight);
        vh.AddVert(vertex);

        vertex.position = new Vector3(xPos + cellWidht, yPos);
        vh.AddVert(vertex);

        float widhtSqr = thickness * thickness;
        float distanceSqr = widhtSqr / 2f;
        float distance = Mathf.Sqrt(distanceSqr);

        //Vertex composant l'intérieur du cadre
        vertex.position = new Vector3(xPos + distance, yPos + distance);
        vh.AddVert(vertex);

        vertex.position = new Vector3(xPos + distance, yPos + (cellHeight - distance));
        vh.AddVert(vertex);

        vertex.position = new Vector3(xPos + (cellWidht - distance), yPos + (cellHeight - distance));
        vh.AddVert(vertex);

        vertex.position = new Vector3(xPos + (cellWidht - distance), yPos + distance);
        vh.AddVert(vertex);

        int offset = index * 8;

        //Left Edge
        vh.AddTriangle(offset + 0, offset + 1, offset + 5);
        vh.AddTriangle(offset + 5, offset + 4, offset + 0);

        //Top Edge
        vh.AddTriangle(offset + 1, offset + 2, offset + 6);
        vh.AddTriangle(offset + 6, offset + 5, offset + 1);

        //Right Edge
        vh.AddTriangle(offset + 2, offset + 3, offset + 7);
        vh.AddTriangle(offset + 7, offset + 6, offset + 2);

        //Bottom Edge
        vh.AddTriangle(offset + 3, offset + 0, offset + 4);
        vh.AddTriangle(offset + 4, offset + 7, offset + 3);

        
    }
}

