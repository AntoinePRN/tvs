/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseButtonManager : MonoBehaviour
{
    private WorkFlowManager wm;

    [Header("Les boutons")]
    [SerializeField] private Button playButton = null;
    [SerializeField] private Button pauseButton = null;

    private void Awake()
    {
        wm = WorkFlowManager.instance;
        //On active d'abord les boutons nécessaire
        playButton.gameObject.SetActive(!wm.isSimulationRunning);
        pauseButton.gameObject.SetActive(wm.isSimulationRunning);

        playButton.onClick.AddListener(delegate { Play(); });
        pauseButton.onClick.AddListener(delegate { Pause(); });
    }

    private void Update()
    {
        //Si la simulation tourne et que le boutoin play est actif on a un probleme 
        if (wm.isSimulationRunning && playButton.IsActive())
        {
            playButton.gameObject.SetActive(false);
            pauseButton.gameObject.SetActive(true);
        }

        if(!wm.isSimulationRunning && !playButton.IsActive())
        {
            playButton.gameObject.SetActive(true);
            pauseButton.gameObject.SetActive(false);
        }
    }

    private void Play()
    {
        wm.ResumeSimulation();
        playButton.gameObject.SetActive(false);
        pauseButton.gameObject.SetActive(true);
    }

    private void Pause()
    {
        wm.PauseSimulation();
        playButton.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(false);

        //Permet d'éviter le bug dans lequel les graphiques se mettent à zéro en activant la pause depuis l'écran du workflow
        if(SceneManager.GetActiveScene().name == "WorkFlowScene")
        {
            wm.indexButton = wm.dayNumber;
        }
    }

}
