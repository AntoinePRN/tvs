/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;
using UnityEngine.UI;

public class HelpPanelManager : MonoBehaviour
{
    [Header("Panneau et texte")]
    public GameObject panelToActivate; //Le panneau qui est le même pour tous 
    public Text textInPanel; //Le texte à l'intérieur du panneau
    public ScrollRect scroll;
    [Header("Statut du bouton")]
    public WorkFlowManager.Status status; //Le status du bouton

    private Button button; //réference privé au bouton
    private Vector2 initPos;

    private void Awake()
    {
        //Comme le bouton est forcément sur le même objet que le script on vient le récuperer directement 
        button = this.gameObject.GetComponent<Button>();
        //Puis on ajoute le Lisener au bouton
        button.onClick.AddListener(delegate { OnClick(); });
        initPos = scroll.content.anchoredPosition;
    }

    /**
     * Tout les status : 
        S, E, IA, ISAD, NH, HC, HR, DC, Rsymp, Rasymp, Vs, Ve
    */
    private void OnClick()
    {
        //On va venir distinguer les cas en fonction de l'origine du bouton
        //On viendra alors modifier le texte en fonction 
        switch (status)
        {
            case WorkFlowManager.Status.S:
                textInPanel.text = "Compartiment S : individus Sains (appelés \"susceptibles\") et pouvant être contaminés.";
                break;
            case WorkFlowManager.Status.E:
                textInPanel.text = "Compartiment E : individus ayant été Exposés au virus (c'est à dire contaminés). Ils sont infectés mais pas contagieux.";
                break;
            case WorkFlowManager.Status.IA:
                textInPanel.text = "Compartiment IAsymp : individus Infectieux mais Asymptomatiques (ne seront pas hospitalisés).";
                break;
            case WorkFlowManager.Status.ISAD:
                textInPanel.text = "Compartiment ISymp : individus Infectieux et Symptomatiques (pourront être hospitalisés pour certains).";
                break;
            case WorkFlowManager.Status.NH:
                textInPanel.text = "Compartiment NH : individus symptomatiques Non Hospitalisés.";
                break;
            case WorkFlowManager.Status.HC:
                textInPanel.text = "HC : individus en Hospitalisation Classique (c'est à dire sans nécessité de passage en service de réanimation  ou de soins intensifs).";
                break;
            case WorkFlowManager.Status.HR:
                textInPanel.text = "HR : individus Hospitalisés et nécessitant un passage en service de Réanimation ou de soins intensifs.";
                break;
            case WorkFlowManager.Status.DC:
                textInPanel.text = "Compartiment DC : individus Décédés.";
                break;
            case WorkFlowManager.Status.Rasymp:
                textInPanel.text = "Compartiment RAsymp : individus Rétablis après avoir été Asymptomatiques. Ne peuvent plus être infectés ensuite dans ce modèle, mais pourront être vaccinés car ils n'ont pas forcément été identifiés comme ayant eu la maladie.";
                break;
            case WorkFlowManager.Status.Rsymp:
                textInPanel.text = "Compartiment RSymp : individus Rétablis après avoir été Symptomatiques. Ne peuvent plus être infectés ensuite dans ce modèle et ne seront pas vaccinés.";
                break;
            case WorkFlowManager.Status.Vs:
                textInPanel.text = "Compartiment VS : individus retirés de S car Vaccinés et chez qui le vaccin s'est montré efficace. Ces individus ne peuvent plus être infectés ensuite dans ce modèle.";
                break;
            case WorkFlowManager.Status.Ve:
                textInPanel.text = "Compartiment VE : individus qui ont été Exposés (mais non symptomatiques) puis Vaccinés et chez qui le vaccin s'est montré efficace. Ces individus ne peuvent plus être infectés ensuite dans ce modèle.";
                break;
            default:
                Debug.LogError("Problème au niveau du switch");
                break;
        }
        //Une fois le texte modifé, on active le panneau
        panelToActivate.SetActive(true);
        scroll.content.anchoredPosition = initPos;
    }
}
