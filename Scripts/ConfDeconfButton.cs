/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;
using UnityEngine.UI;

public class ConfDeconfButton : MonoBehaviour
{
    private WorkFlowManager wm;

    [Header("Bloc bouton confinement")]
    public Button confineButton;
    public Button deconfineButton;

    // Start is called before the first frame update
    void Start()
    {
        wm = WorkFlowManager.instance;
        confineButton.gameObject.SetActive(!wm.modeConfinementBouton);
        confineButton.onClick.AddListener(delegate { ConfineButtonListener(); });
        deconfineButton.gameObject.SetActive(wm.modeConfinementBouton);
        deconfineButton.onClick.AddListener(delegate { ConfineButtonListener(); });
    }

    private void ConfineButtonListener()
    {
        wm.modeConfinementBouton = !wm.modeConfinementBouton;
    }

    private void Update()
    {
        if (wm.modeConfinementProgramme)
        {
            confineButton.interactable = false;
            deconfineButton.interactable = false;
            confineButton.gameObject.SetActive(!wm.confinementIsRunning);
            deconfineButton.gameObject.SetActive(wm.confinementIsRunning);
        } else
        {
            confineButton.interactable = true;
            deconfineButton.interactable = true;
            confineButton.gameObject.SetActive(!wm.modeConfinementBouton);
            deconfineButton.gameObject.SetActive(wm.modeConfinementBouton);
        }
            
    }
}
