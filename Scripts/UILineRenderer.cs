/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/**
 * Script permettant de génerer une ligne en se basant sur une liste de points
 * Elle est de plus en lien direct avec la grille afin de garder une echelle cohérente
 */
public class UILineRenderer : Graphic
{
    [Header("Grille")]
    public Vector2Int gridSize;
    public UIGridRenderer grid;

    [Header("Attribut de la courbe")]
    public float thickness = 10f;

    private List<Vector2> points = new List<Vector2>();

    //Tout ces attributs vont nous servir à tracer la courbe
    private float width;
    private float height;
    private float unitWidth;
    private float unitHeight;

    protected override void OnPopulateMesh(VertexHelper vh)
    {

        vh.Clear(); //HYPER IMPORTANT

        width = rectTransform.rect.width;
        height = rectTransform.rect.height;

        unitWidth = width / gridSize.x;
        unitHeight = height / gridSize.y;

        if (points.Count < 2) return;


        float angle = 0;
        for (int i = 0; i < points.Count - 1; i++)
        {

            Vector2 point = points[i];
            Vector2 point2 = points[i + 1];

            if (i < points.Count - 1)
            {
                angle = GetAngle(points[i], points[i + 1]) + 90f;
            }

            DrawVerticesForPoint(point, point2, angle, vh);
        }

        for (int i = 0; i < points.Count - 1; i++)
        {
            int index = i * 4;
            vh.AddTriangle(index + 0, index + 1, index + 2);
            vh.AddTriangle(index + 1, index + 2, index + 3);
        }
    }

    public float GetAngle(Vector2 me, Vector2 target)
    {
        return (float)(Mathf.Atan2(406f * (target.y - me.y), 667f * (target.x - me.x)) * (180 / Mathf.PI));
    }

    void DrawVerticesForPoint(Vector2 point, Vector2 point2, float angle, VertexHelper vh)
    {
        UIVertex vertex = UIVertex.simpleVert;
        vertex.color = color;

        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(-thickness / 2, 0);
        vertex.position += new Vector3(unitWidth * point.x, unitHeight * point.y);
        vh.AddVert(vertex);

        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(thickness / 2, 0);
        vertex.position += new Vector3(unitWidth * point.x, unitHeight * point.y);
        vh.AddVert(vertex);

        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(-thickness / 2, 0);
        vertex.position += new Vector3(unitWidth * point2.x, unitHeight * point2.y);
        vh.AddVert(vertex);

        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(thickness / 2, 0);
        vertex.position += new Vector3(unitWidth * point2.x, unitHeight * point2.y);
        vh.AddVert(vertex);
    }

    /**
     * Cette méthode va permettre d'ajouter directement une liste entière qu'on aura récuperer depuis le dataSaver
     * Cela evitera d'ajouter point par point
     * On va de plus automatiquement ajuster l'echelle y en fonction du la valeur max de la liste
     */
    public void AddEntireList(List<int> effectiveFromDataSaver)
    {
        points.Clear();
        int index = 0;
        foreach (int value in effectiveFromDataSaver)
        {
            Vector2 newPoint = new Vector2(index, value);
            points.Add(newPoint);
            index++;
        }
        SetVerticesDirty();
    }

}
