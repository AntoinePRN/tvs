/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine.UI;
using UnityEngine;
using System;

public class Panel3Manager : MonoBehaviour
{
    private WorkFlowManager wm;

    [Header("Labels")]
    public Text cumulConfLabel;
    public Text cumulHopitalLabel;
    public Text dcLabel;
    public Text vTotLabel;

    private void Start()
    {
        wm = WorkFlowManager.instance;
    }
    // Update is called once per frame
    void Update()
    {
        cumulConfLabel.text = wm.cumulJourEnConfinement.ToString();

        int cumulPassageHoptial = (int)Math.Round(wm.cumulPassageHopital);
        cumulHopitalLabel.text = cumulPassageHoptial.ToString();

        int cumulDeces = (int)Math.Round(wm.effective[WorkFlowManager.Status.DC]);
        dcLabel.text = cumulDeces.ToString();

        int cumulVaccin = (int)Math.Round(wm.effective[WorkFlowManager.Status.Vs] + wm.effective[WorkFlowManager.Status.Ve]);
        vTotLabel.text = cumulVaccin.ToString();
    }
}
