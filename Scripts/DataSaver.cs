/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using UnityEngine;

/**
 * Ce script va servir "d'historique"
 * Il a pour but de garder en mémoire les valeurs de chaque compartiment au fur et à mesure du temps
 */
public class DataSaver : MonoBehaviour
{
    private WorkFlowManager wm;

    private IDictionary<WorkFlowManager.Status, List<int>> allData = new Dictionary<WorkFlowManager.Status, List<int>>();

    // Start is called before the first frame update
    void Start()
    {
        wm = WorkFlowManager.instance;

        //On passe ici par un foreach pour garder un contexte très general, si jamais on vient rajouter des compartiment par la suite
        foreach(KeyValuePair<WorkFlowManager.Status, double> entry in wm.effective)
        {
            allData.Add(entry.Key, new List<int>());
        }
    }

    /**
     * Cette méthode sera appelé chaque "jour" quand le workflowanager vient se rafraichir
     */
    public void AddNewValue(IDictionary<WorkFlowManager.Status, double> effective)
    {
        foreach (KeyValuePair<WorkFlowManager.Status, double> entry in effective)
        {
            //On convertit dans un premier temps le double en int
            int entryValueInt = (int)Math.Round(entry.Value);

            //Puis on vent ajouter pour chaque compart
            allData[entry.Key].Add(entryValueInt);
        }
    }

    /**
     * Cette méthode permet simplement de récuperer une liste de valeur en spécifiant un status particulier
     */
    public List<int> GetValueListFromStatus(WorkFlowManager.Status s) => allData[s];

    /**
     * Permet de remettre à zero l'historique
     */
    public void ClearAll()
    {
        allData = new Dictionary<WorkFlowManager.Status, List<int>>();
        foreach (KeyValuePair<WorkFlowManager.Status, double> entry in wm.effective)
        {
            allData.Add(entry.Key, new List<int>());
        }
    }
}
