/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;
using UnityEngine.UI;

public class CreditsManager : MonoBehaviour
{
    public Button display;
    public Button hide;
    public GameObject panel;

    private void Start()
    {
        display.onClick.AddListener(delegate { TogglePanel(); });
        hide.onClick.AddListener(delegate { TogglePanel(); });
    }

    private void TogglePanel()
    {
        panel.SetActive(!panel.activeSelf);
    }
}
