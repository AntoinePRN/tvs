/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Globalization;

public class SettingsManager : MonoBehaviour
{
    private WorkFlowManager wm;

    [Header("Tous les boutons")]
    public Button returnButton; //Button qui permet de revenir au workflow depuis les parametres
    public Button resetAll;

    [Header("Tous les toggles")]
    public Toggle mesureBarriere;
    public Toggle vaccination;
    public Toggle confinement;
    public Toggle variant;

    [Header("Tous les InputField")]
    public InputField sPopInput;
    public InputField ePopInput;

    public InputField dateDebutMesuresBarrieresInput;
    public InputField dateFinMesuresBarrieresInput;

    public InputField dateDebutVaccinationInput;
    public InputField dureeVaccinationInput;

    public InputField dateDebutPremierConfinementInput;
    public InputField NbConfinementInput;
    public InputField dureeConfinementInput;
    public InputField dureeEntreConfinementInput;

    public InputField dateDebutVariant;
    public InputField facteurContagiositeVariant; //Attention on attend ici un décimal et non un entier !

    [Header("Panneau pour cacher certains param")]
    //Panel qui permet de ne pas accéder au compartiment de pop pdt la simulation
    public GameObject populationHidePanel;
    public GameObject mesureBarriereHidePanel;
    public GameObject vaccinationHidePanel;
    public GameObject confinementHidePanel;
    public GameObject variantHidePanel;

    [Header("Panneau d'erreur")]
    public GameObject errorPanel;
    public Text errorText;


    // Start is called before the first frame update
    void Start()
    {
        wm = WorkFlowManager.instance;

        /**  _______
         *  |BUTTONS|
         */
        returnButton.onClick.AddListener(delegate { ApplyModification(); });
        resetAll.onClick.AddListener(delegate { ResetValue(); });

        /**  ___________
         *  |INPUTFIELDS|
         */

        sPopInput.text = wm.Spop.ToString();
        ePopInput.text = wm.Epop.ToString();

        dateDebutMesuresBarrieresInput.text = wm.dateDebutMesuresBarriere.ToString();
        dateFinMesuresBarrieresInput.text = wm.dateFinMesuresBarriere.ToString();

        dateDebutVaccinationInput.text = wm.jourDebutVaccination.ToString();
        dureeVaccinationInput.text = (wm.jourFinVaccination - wm.jourDebutVaccination).ToString();

        dateDebutPremierConfinementInput.text = wm.dateDebutPremierConfinement.ToString();
        NbConfinementInput.text = wm.nombreDeConfinements.ToString();
        dureeConfinementInput.text = wm.dureeDesConfinements.ToString();
        dureeEntreConfinementInput.text = wm.dureeEntreDeuxConfinements.ToString();

        dateDebutVariant.text = wm.dateDebutVariant.ToString();
        facteurContagiositeVariant.text = wm.facteurContagiosite.ToString();

        /**  ______
         *  |TOGGLE|
         */

        //On ajoute ici le listener pour les mesures barrières
        //Commme il s'agit d'un toggle le return possède une valeur booléenne
        mesureBarriere.isOn = wm.modeMesureBarriere;
        mesureBarriere.onValueChanged.AddListener((value) => EtatMesureBarriere(value));
        vaccination.isOn = wm.modeVaccination;
        vaccination.onValueChanged.AddListener((value) => EtatVaccination(value));
        confinement.isOn = wm.modeConfinementProgramme;
        confinement.onValueChanged.AddListener((value) => EtatConfinement(value));
        variant.isOn = wm.modeVariant;
        variant.onValueChanged.AddListener((value) => EtatVariant(value));

        /**  _____
         *  |PANEL|
         */
        mesureBarriereHidePanel.SetActive(!wm.modeMesureBarriere);
        confinementHidePanel.SetActive(!wm.modeConfinementProgramme);
        vaccinationHidePanel.SetActive(!wm.modeVaccination);
        variantHidePanel.SetActive(!wm.modeVariant);
    }

    /**
     * Cette méthode va permettre de réinitialiser tous les champs 
     */
    private void ResetValue()
    {
        wm.Spop = 67000000.0;
        wm.Epop = 10.0;
        sPopInput.text = wm.Spop.ToString();
        ePopInput.text = wm.Epop.ToString();

        wm.modeMesureBarriere = false;
        wm.dateDebutMesuresBarriere = 100;
        wm.dateFinMesuresBarriere = 1800;

        mesureBarriere.isOn = false;
        mesureBarriereHidePanel.SetActive(true);
        dateDebutMesuresBarrieresInput.text = wm.dateDebutMesuresBarriere.ToString();
        dateFinMesuresBarrieresInput.text = wm.dateFinMesuresBarriere.ToString();


        wm.modeConfinementProgramme = false;
        wm.dateDebutPremierConfinement = 100;
        wm.nombreDeConfinements = 3;
        wm.dureeDesConfinements = 45;
        wm.dureeEntreDeuxConfinements = 120;

        confinement.isOn = false;
        confinementHidePanel.SetActive(true);
        dateDebutPremierConfinementInput.text = wm.dateDebutPremierConfinement.ToString();
        NbConfinementInput.text = wm.nombreDeConfinements.ToString();
        dureeConfinementInput.text = wm.dureeDesConfinements.ToString();
        dureeEntreConfinementInput.text = wm.dureeEntreDeuxConfinements.ToString();

        wm.modeVaccination = false;
        wm.jourDebutVaccination = 390;
        wm.jourFinVaccination = 750;

        vaccination.isOn = false;
        vaccinationHidePanel.SetActive(true);
        dateDebutVaccinationInput.text = wm.jourDebutVaccination.ToString();
        dureeVaccinationInput.text = (wm.jourFinVaccination - wm.jourDebutVaccination).ToString();

        wm.modeVariant = false;
        wm.dateDebutVariant = 390;
        wm.facteurContagiosite = 1.6f;

        variant.isOn = false;
        variantHidePanel.SetActive(true);
        dateDebutVariant.text = wm.dateDebutVariant.ToString();
        facteurContagiositeVariant.text = wm.facteurContagiosite.ToString();
    }


    /**
     * Méthode permettant d'activer le mode mesure barrière avec le toggle des paramètres
     */
    private void EtatMesureBarriere(bool state)
    {
        mesureBarriereHidePanel.SetActive(!state);
        wm.modeMesureBarriere = state;
    }
    /**
     * Méthode permettant d'activer le mode vaccination avec le toggle des paramètres
     */
    private void EtatVaccination(bool state)
    {
        vaccinationHidePanel.SetActive(!state);
        wm.modeVaccination = state;
    }
    /**
     * Méthode permettant d'activer le mode cofnienment avec le toggle des paramètres
     */
    private void EtatConfinement(bool state)
    {
        confinementHidePanel.SetActive(!state);
        wm.modeConfinementProgramme = state;
    }
    /**
     * Méthode permettant d'activer le mode variant avec le toggle des paramètres
     */
    private void EtatVariant(bool state)
    {
        variantHidePanel.SetActive(!state);
        wm.modeVariant = state;
    }


    /*
     * Cette méthode va permettre d'appliquer toute les modifs de l'utilisateurs
     * On va prendre en compte toutes les erreurs possibles ...
     * On va considérer pour le moment que si on détetcte une erreur dans le bloc on le réinitialise
     * ça va éviter de  distinguer trop de cas
     */
    private void ApplyModification()
    {
        //Chaque boolean va vérifier les différents blocs
        bool mesureBarriereOk = true;
        bool popIsOk = true;
        bool vaccinationIsOk = true;
        bool confinementIsOk = true;
        bool variantisOk = true;

        //Vérification des populations initiales
        //On remet les valeurs par défauts si jamais une erreur survient
        try
        {
            if (int.Parse(sPopInput.text) < 0 || int.Parse(ePopInput.text) < 0)
            {
                popIsOk = false;
                errorText.text = "Populations initiales invalides";
            }
            else
            {
                wm.Spop = int.Parse(sPopInput.text);
                wm.Epop = int.Parse(ePopInput.text);
            }
        }
        catch (OverflowException) //On récupere l'erreur spécifique au dépassement de taille du int
        {
            popIsOk = false;
            errorText.text = "Populations initiales trop grandes";
        }

        //Ce bloc concerne la vérification des paramètres des mesures barrières
        if (wm.modeMesureBarriere)
        {
            try
            {
                int dateDebutMesureBarrière = int.Parse(dateDebutMesuresBarrieresInput.text); ;
                int dateFinMesureBarrière = int.Parse(dateFinMesuresBarrieresInput.text);

                if (dateFinMesureBarrière <= dateDebutMesureBarrière
                || dateDebutMesureBarrière < 0
                || dateFinMesureBarrière <= 0)
                {
                    mesureBarriereOk = false;
                    errorText.text = "Jour de début et/ou fin de mesures barrières invalides";
                }
                else
                {
                    wm.dateDebutMesuresBarriere = dateDebutMesureBarrière;
                    wm.dateFinMesuresBarriere = dateFinMesureBarrière;
                }
            }
            catch (OverflowException)
            {
                mesureBarriereOk = false;
                errorText.text = "Jour de début et/ou fin de mesures barrières trop grand";
            }
           
        }

        //Le  bloc du confinement 
        if (wm.modeConfinementProgramme)
        {
            try
            {
                int datePremierConf = int.Parse(dateDebutPremierConfinementInput.text);
                int nbConf = int.Parse(NbConfinementInput.text);
                int dureeConf = int.Parse(dureeConfinementInput.text);
                int dureeEntreConf = int.Parse(dureeEntreConfinementInput.text);

                if (datePremierConf < 0
                    || nbConf <= 0
                    || dureeConf <= 0
                    || dureeEntreConf <= 0)
                {
                    confinementIsOk = false;
                    errorText.text = "Jour de début et/ou durées de confinements invalides";
                }
                else
                {
                    wm.dateDebutPremierConfinement = datePremierConf;
                    wm.nombreDeConfinements = nbConf;
                    wm.dureeDesConfinements = dureeConf;
                    wm.dureeEntreDeuxConfinements = dureeEntreConf;
                }

            } catch (OverflowException)
            {
                confinementIsOk = false;
                errorText.text = "Jour de début et/ou durées de confinements trop grand";
            }
            
        }

        //Puis finalement la vaccination 
        if (wm.modeVaccination)
        {
            try
            {
                //on crée des variables locales pour plus de simplicité
                int dateDebutVaccination = int.Parse(dateDebutVaccinationInput.text);
                int dureeVaccination = int.Parse(dureeVaccinationInput.text);

                if (dateDebutVaccination < 0 || dureeVaccination < 0)
                {
                    vaccinationIsOk = false;
                    errorText.text = "Jour de début et/ou durée de vaccination invalides";
                }
                else
                {
                    wm.jourDebutVaccination = dateDebutVaccination;
                    wm.jourFinVaccination = dateDebutVaccination + dureeVaccination;
                    wm.CalculDureeVaccination();
                }

            }
            catch (OverflowException)
            {
                vaccinationIsOk = false;
                errorText.text = "Jour de début et/ou durée de vaccination trop grand";
            }
        }

        if (wm.modeVariant)
        {
            try
            {
                int dateDebutVariantInt = int.Parse(dateDebutVariant.text);
                string txtWithComa = facteurContagiositeVariant.text.Replace(',', '.'); //On remplace les virgules par des points pour la conversion
                float facteurContaFloat = float.Parse(txtWithComa, new CultureInfo("en-US").NumberFormat);
                Debug.Log(facteurContaFloat);

                if (dateDebutVariantInt < 0 || facteurContaFloat <= 0)
                {
                    variantisOk = false;
                    errorText.text = "Jour de début et/ou facteur du variant invalides";
                }
                else
                {
                    wm.dateDebutVariant = dateDebutVariantInt;
                    wm.facteurContagiosite = facteurContaFloat;
                }
            }
            catch (Exception e)
            {
                variantisOk = false;
                errorText.text = e.ToString();
            }
        }

        

        //Si toutes les modifs sont ok on retourne sur le workflow
        if(mesureBarriereOk && popIsOk && vaccinationIsOk && confinementIsOk && variantisOk)
        {
            SceneManager.LoadScene("WorkFlowScene");
            //Si on est jour 0 et que la simulation ne tourne pas cela signifie qu'one est au début donc on veut restart en revenant
            //sur le workflow pou rmettre à jour les poop initiales
            if(wm.dayNumber == 0 && wm.isSimulationRunning == false)
            {
                wm.RestartButton();
            }
        } else //Sinon on affiche les problèmes 
        {
            errorPanel.SetActive(true);
        }
    }

}


