/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;


public class SceneLoader : MonoBehaviour
{
    //Le nom de la scene qu'on souhaite charger
    public string sceneToLoad;

    //Réference privé au bouton, on va le récuperer via une fonction dans le Awake
    private Button button;

    private void Awake()
    {
        button = this.gameObject.GetComponent<Button>();
        button.onClick.AddListener(delegate { OnClick(); });
    }

    //La méthpde qui sera invoqué par un appui de bouton par exemple
    public void OnClick()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
