/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine.UI;
using UnityEngine;
using System;

public class UIManager : MonoBehaviour
{

    [Header("UI Settings")]
    public Text sLabel;
    public Text eLabel;
    public Text isadLabel;
    public Text iaLabel;
    public Text nhLabel;
    public Text hcLabel;
    public Text hrLabel;
    public Text dcLabel;
    public Text rsympLabel;
    public Text rasympLabel;
    public Text vsLabel;
    public Text veLabel;

    private WorkFlowManager wm;

    private void Awake()
    {
        wm = WorkFlowManager.instance;
    }

    // Update is called once per frame
    void Update()
    {

        double sFloat = wm.effective[WorkFlowManager.Status.S];
        int sDisplay = (int)Math.Round(sFloat);
        sLabel.text = sDisplay.ToString();
        //sLabel.text = sFloat.ToString();

        double eFloat = wm.effective[WorkFlowManager.Status.E];
        int eDisplay = (int)Math.Round(eFloat);
        eLabel.text = eDisplay.ToString();
        //eLabel.text = eFloat.ToString();

        double isadFloat = wm.effective[WorkFlowManager.Status.ISAD];
        int isadDisplay = (int)Math.Round(isadFloat);
        isadLabel.text = isadDisplay.ToString();
        //isadLabel.text = isadFloat.ToString();

        double iaFloat = wm.effective[WorkFlowManager.Status.IA];
        int iaDisplay = (int)Math.Round(iaFloat);
        iaLabel.text = iaDisplay.ToString();
        //iaLabel.text = iaFloat.ToString();

        double nhFLoat = wm.effective[WorkFlowManager.Status.NH];
        int nhDisplay = (int)Math.Round(nhFLoat);
        nhLabel.text = nhDisplay.ToString();

        double hcFloat = wm.effective[WorkFlowManager.Status.HC];
        int hcDisplay = (int)Math.Round(hcFloat);
        hcLabel.text = hcDisplay.ToString();

        double hrFloat = wm.effective[WorkFlowManager.Status.HR];
        int hrDisplay = (int)Math.Round(hrFloat);
        hrLabel.text = hrDisplay.ToString();

        double dcFloat = wm.effective[WorkFlowManager.Status.DC];
        int dcDisplay = (int)Math.Round(dcFloat);
        dcLabel.text = dcDisplay.ToString();


        double rsympFloat = wm.effective[WorkFlowManager.Status.Rsymp];
        int rsympDisplay = (int)Math.Round(rsympFloat);
        rsympLabel.text = rsympDisplay.ToString();

        double rasympFloat = wm.effective[WorkFlowManager.Status.Rasymp];
        double rasympDisplay = (int)Math.Round(rasympFloat);
        rasympLabel.text = rasympDisplay.ToString();

        double vsFloat = wm.effective[WorkFlowManager.Status.Vs];
        int vsDisplay = (int)Math.Round(vsFloat);
        vsLabel.text = vsDisplay.ToString();

        double veFloat = wm.effective[WorkFlowManager.Status.Ve];
        int veDisplay = (int)Math.Round(veFloat);
        veLabel.text = veDisplay.ToString();
    }
}
