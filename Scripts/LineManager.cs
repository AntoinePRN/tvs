/**
    ToyVirusSimulation
   Copyright (C) 2021  Piron Antoine

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LineManager : MonoBehaviour
{
    private WorkFlowManager wm;
    private DataSaver data;

    [Header("Courbe et status associé")]
    public List<UILineRenderer> allCurves;
    public List<WorkFlowManager.Status> displayStatus;

    [Header("Echelle pour les courbes")]
    public int echelleX = 1;
    public float echelleY = 1;

    [Header("Mode d'échelle")]
    public bool echelleAuto = false;

    [Header("Labels")]
    public Text yLabel;
    public Text xRight;
    public Text xLeft;

    [Header("Boutons de déplacements")]
    public Button rightButton;
    public Button leftButton;
    public int step = 5;

    //Attribut pour la conversion de la courbe
    private float maxInAllList = 0f;
    private float desiredMax = 0f;

    // Start is called before the first frame update
    void Start()
    {
        //on récupere notre instance du workflow
        wm = WorkFlowManager.instance;

        data = GameObject.Find("WorkFlowManager").GetComponent<DataSaver>();

        rightButton.onClick.AddListener(delegate { Move(true);});
        leftButton.onClick.AddListener(delegate { Move(false);});

        //On met ici quelques print de debug si jamais une erreur surrvient
        // Mais normalement on n'aura pas à lire ces mots doux ;)
        if(data == null)
        {
            Debug.LogError("Le datasaver n'as pas pu être recuperer");
        }
        if (allCurves.Count != displayStatus.Count)
        {
            Debug.LogError("Nombre de courbe ne correspond pas avec le nombre de status à affficher");
        }

        InvokeRepeating(nameof(UpdateInterval), WorkFlowManager.HIGH_SPEED, WorkFlowManager.HIGH_SPEED);
    }

    private void Update()
    {
        if(wm.dayNumber == 0)
        {
            maxInAllList = 0;
            desiredMax = 0;
        }
        yLabel.text = maxInAllList.ToString();
    }

    private void UpdateInterval()
    { 
        for (int i = 0; i<allCurves.Count; i++)
        {
            if(data.GetValueListFromStatus(displayStatus[i]).Count > 0)
            {
                //On récupère d'abord le max, toutes courbes confondues
                maxInAllList = data.GetValueListFromStatus(displayStatus[i]).Max() > maxInAllList ? data.GetValueListFromStatus(displayStatus[i]).Max() : maxInAllList;
            }
            
        }

        for(int i = 0; i<allCurves.Count; i++)
        {
            //On désactive les boutons de défilements lors de la simulation si jamais ils ne le sont pas déjà 
            if (wm.isSimulationRunning & rightButton.interactable && leftButton.interactable)
            {
                rightButton.interactable = false;
                leftButton.interactable = false;
            }
            //On active les boutons de défilement lors de la pause si jamais ils ne le sont pas déjà
            else if (!wm.isSimulationRunning & !rightButton.interactable && !leftButton.interactable)
            {
                rightButton.interactable = true;
                leftButton.interactable = true;
                //On définit l'index de base pour le défilement avec les boutons ssi c'est possible de défiler => jour actuelle supérieur à l'echelle
                //On vérifie dans ce bloc car le firstIndex viendra alors de modifier uniquement lors de la mise en pause.
                wm.indexButton = data.GetValueListFromStatus(displayStatus[i]).Count < allCurves[i].gridSize.x ? 0 : data.GetValueListFromStatus(displayStatus[i]).Count - allCurves[i].gridSize.x;
            }

            List<int> newEffective = new List<int>();
            //Si le nmbre de jour est inférieur à la taille du tableau en x 
            if(data.GetValueListFromStatus(displayStatus[i]).Count < allCurves[i].gridSize.x)
            {
                xLeft.text = "0";
                xRight.text = allCurves[i].gridSize.x.ToString();
                if(data.GetValueListFromStatus(displayStatus[i]).Count > 0)
                {
                    //On réadapte ensuite l'échelle en Y si jamais on a dénfinit une echelle auto -> panel 4
                    echelleY = echelleAuto ? (maxInAllList / allCurves[i].grid.gridSize.y) : echelleY;
                    //On calcul ensuite le max qu'on peut afficher sur le graph
                    desiredMax = allCurves[i].grid.gridSize.y * echelleY;
                    //finalement on convertit notre effectif à l'aide du converter définit plus bas 
                    newEffective = data.GetValueListFromStatus(displayStatus[i]).ConvertAll(new System.Converter<int, int>(ConvertEffectiveInt));
                }
            }
            else //Si il y a plus de jour que de taille en x -> on va décaler la courbe 
            {
                //Le raisonnement ici est sensiblement le même on travail simplement sur un sous effecitifs
                //On définit le premier index soit automatiqueent soit manuellement avec les boutons de défilements
                int firstIndex = rightButton.interactable ? wm.indexButton : data.GetValueListFromStatus(displayStatus[i]).Count - allCurves[i].gridSize.x;
                //On vient controller ici que l'index n'est pas trop grand si jamais il est geré par l'utilisateur
                if(firstIndex > data.GetValueListFromStatus(displayStatus[i]).Count - allCurves[i].gridSize.x)
                {
                    //On remet la valeur max le cas échéant
                    firstIndex = data.GetValueListFromStatus(displayStatus[i]).Count - allCurves[i].gridSize.x;
                    //On remet aussi l'index de l'utilisateur a la bonne valleur pour eviter qu'il soit trop haut et que l'utilisateur doivent cliquer bcp de fois en arrière sans effet
                    wm.indexButton = firstIndex;
                }
                xLeft.text = firstIndex.ToString();
                xRight.text = (allCurves[i].gridSize.x + firstIndex).ToString();
                List<int> subEffective = data.GetValueListFromStatus(displayStatus[i]).GetRange(firstIndex, allCurves[i].gridSize.x);
                if (subEffective.Count > 0)
                {
                    echelleY = echelleAuto ? (maxInAllList / allCurves[i].grid.gridSize.y) : echelleY;
                    desiredMax = allCurves[i].grid.gridSize.y * echelleY;
                    newEffective = subEffective.ConvertAll(new System.Converter<int, int>(ConvertEffectiveInt));
                }
            }
            echelleY = echelleY < 100 ? 100 : echelleY; //ajout d'une echelle minimale pour éviter bug d'affichage
            allCurves[i].gridSize = new Vector2Int(echelleX * allCurves[i].grid.gridSize.x, (int)echelleY * allCurves[i].grid.gridSize.y);
            allCurves[i].AddEntireList(newEffective);
        }
    }

    /**
     * Permet de convertir un entier entre [0,maxInList] e un entier entre [0,desiredMax]
     */
    private int ConvertEffectiveInt(int number)
    {
        float retNumber = 0;
        //Pour éviter une division par 0 -> si le maxInAllList vaut 0 alors on veut return 0
        if (maxInAllList != 0)
        {
             retNumber = (float)(number * desiredMax) / maxInAllList;
        }
        
        return (int)retNumber;
    }

    /**
     * Méthode permettant de déplacer l'index pour la visualisation du graph à l'arret
     * true => droite / false => gauche
     * On pense bien à arreter la méthode d'update pour éviter de dessiner plusieur courbe en décalé
     */ 
    private void Move(bool direction)
    {
        CancelInvoke();
        //On applique la modification de l'index
        wm.indexButton = direction ? wm.indexButton += step : wm.indexButton -= step;
        //On vient eviter d'obtenir un index négatif
        wm.indexButton = wm.indexButton >= 0 ? wm.indexButton : 0;
        InvokeRepeating(nameof(UpdateInterval), WorkFlowManager.HIGH_SPEED, WorkFlowManager.HIGH_SPEED);
    }


}
