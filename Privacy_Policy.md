# Privacy policy v 1.1 #

## ========================================== ##
Privacy policy for ToyVirusSim

ToyVirusSim runs locally on the device and does not need a network connection.

The developer of ToyVirusSim does not collect any data.

ToyVirusSim uses the core Unity development platform. Thus Unity collects information (device identifiers, version of operating systems, ...), see Section "I play a game that was built with or uses certain Unity software, what should I know?" on page https://unity3d.com/legal/privacy-policy to read the Unity privacy policy.
## ========================================== ##

