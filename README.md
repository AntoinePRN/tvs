# ToyVirusSimulation #

## Warning ##

Subject: Intuitive experimentation of viral spread.

This is a simplified and uncalibrated simulator. It is conditioned by strong simplifying assumptions and does not take into account the random aspects of an epidemic spread.

## I want to check the model ##

If you want to check the model you can consult the "Scripts" folder containing all the C # files that allow us to run the model in the application.
